require 'spec_helper'
require 'alertmanager_helper'

describe 'alertmanager_helper' do
  before do
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, 'recipes:gitlab-alertmanager\\:\\:default')
                          .and_return([stub_node('fqdn' => 'am-1',
                                                 'hostname' => 'am-1'),
                                       stub_node('fqdn' => 'am-2',
                                                 'hostname' => 'am-2')])

    allow(Gitlab).to receive(:private_ips_for_node) do |node|
      case node['hostname']
      when 'am-1'
        ['192.168.1.2']
      when 'am-2'
        ['192.168.1.3']
      else
        []
      end
    end
  end

  let(:alertmanager_module) do
    c = Class.new { include Gitlab::Alertmanager }
    c.new
  end

  it 'can generate a list of peers' do
    peers_search = [{ 'fqdn' => 'am-1', 'hostname' => 'am-1' },
                    { 'fqdn' => 'am-2', 'hostname' => 'am-2' }]
    peer_list = alertmanager_module.get_alertmanager_peers(peers_search)
    expect(peer_list).to eq(['192.168.1.2', '192.168.1.3'])
  end
end
