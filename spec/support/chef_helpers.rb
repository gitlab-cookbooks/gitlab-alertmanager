module ChefHelpers
  def stub_node(attrs)
    address =
      case attrs['hostname']
      when 'am-1'
        '192.168.1.2'
      when 'am-2'
        '192.168.1.3'
      end

    interfaces =
      if address
        { 'eth0' => { 'addresses' => { address => { 'family' => 'inet' } } } }
      else
        {}
      end

    attrs.merge('network' => { 'interfaces' => interfaces })
  end
end
