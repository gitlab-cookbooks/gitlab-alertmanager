require 'spec_helper'

alertmanager_default = {
  'id' => '_default',
  'prometheus-alertmanager' => {
    'alertmanager' => {
      'slack' => {
        'api_url' => 'slack_api',
      },
      'snitch' => {
        'api_key' => 'snitch_key',
      },
      'non_prod_pagerduty' => {
        'service_key' => 'non-prod-pd-service-key',
      },
      'prod_pagerduty' => {
        'service_key' => 'prod-pd-service-key',
      },
      'slack_bridge' => {
        'webhook_url' => 'http://127.0.0.1/alertManagerBridge',
        'bearer_token' => 'slack_bridge_token',
      },
    },
  },
}

describe 'gitlab-alertmanager::default' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'gcs execution' do
    before do
      stub_search(:node, 'recipes:gitlab-alertmanager\\:\\:default') do
        [{ 'fqdn' => 'am-1',
           'hostname' => 'am-1',
           'ipaddress' => '192.168.1.2' }]
      end
      stub_search(:node, 'alertmanager_no_peers') { [] }
      allow(Chef::DataBag).to receive(:load).with('prometheus-alertmanager').and_return('_default_keys' => { 'id' => '_default_keys' })
      allow(ChefVault::Item).to receive(:load).with('alertmanager', '_default').and_return(alertmanager_default)
    end

    context 'with a default chef config' do
      cached(:chef_run) do
        ChefSpec::SoloRunner.new(step_into: ['gitlab_alertmanager_gcs_config']) do |node|
          node.normal['alertmanager']['config']['gcs']['enable'] = true
          node.normal['prometheus-alertmanager']['alertmanager']['chef_vault'] = 'alertmanager'
          node.normal['alertmanager']['non_critical_pagerduty_environments'] = %w(gstg dr)
        end.converge(described_recipe)
      end

      it 'does not create the configuration file' do
        expect(chef_run).to_not create_template('/opt/prometheus/alertmanager/alertmanager.yml')
      end

      it 'downloads and decrypts file' do
        expect(chef_run).to create_gitlab_alertmanager_gcs_config('alertmanager')
      end

      it 'steps into gcs_config' do
        expect(chef_run).to run_execute('get')
        expect(chef_run).to run_execute('decrypt')
      end

      it 'uploads all the alerting templates' do
        expect(chef_run).to create_remote_directory('/opt/prometheus/alertmanager/templates')
      end
    end
  end
end
