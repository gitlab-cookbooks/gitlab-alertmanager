require 'spec_helper'

alertmanager_default = {
  'id' => '_default',
  'prometheus-alertmanager' => {
    'alertmanager' => {
      'slack' => {
        'api_url' => 'https://example.com/slack_api',
      },
      'snitch' => {
        'api_key' => 'snitch_key',
        'env' => {
          'prod' => 'prod_snitch_key',
          'test' => 'test_snitch_key',
        },
      },
      'non_prod_pagerduty' => {
        'service_key' => 'non-prod-pd-service-key',
      },
      'prod_pagerduty' => {
        'service_key' => 'prod-pd-service-key',
      },
      'slo_dr' => {
        'service_key' => 'prod-pd-service-key',
      },
      'slo_gprd_cny' => {
        'service_key' => 'prod-pd-service-key',
      },
      'slo_gprd_main' => {
        'service_key' => 'prod-pd-service-key',
      },
      'slo_non_prod' => {
        'service_key' => 'prod-pd-service-key',
      },
      'slack_bridge' => {
        'webhook_url' => 'http://127.0.0.1/alertManagerBridge',
        'bearer_token' => 'slack_bridge_token',
      },
      'issues' => {
        'gitlab.com/some/project' => 'gitlab_alert_token',
      },
    },
  },
}

describe 'gitlab-alertmanager::default' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    before do
      stub_search(:node, 'recipes:gitlab-alertmanager\\:\\:default') do
        [stub_node('fqdn' => 'am-1',
                   'hostname' => 'am-1',
                   'ipaddress' => '192.168.1.2')]
      end
      stub_search(:node, 'alertmanager_no_peers') { [] }
      allow(Chef::DataBag).to receive(:load).with('prometheus-alertmanager').and_return('_default_keys' => { 'id' => '_default_keys' })
      allow(ChefVault::Item).to receive(:load).with('alertmanager', '_default').and_return(alertmanager_default)
    end

    context 'with a default chef config' do
      cached(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['prometheus-alertmanager']['alertmanager']['chef_vault'] = 'alertmanager'
          node.normal['alertmanager']['non_critical_pagerduty_environments'] = %w(gstg dr)
        end.converge(described_recipe)
      end

      it 'creates a prometheus user and group' do
        expect(chef_run).to create_user('prometheus').with(
          system: true,
          shell: '/bin/false'
        )
      end

      it 'creates the alertmanager dir in the configured location' do
        expect(chef_run).to create_directory('/opt/prometheus/alertmanager').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755',
          recursive: true
        )
      end

      it 'creates the log dir in the configured location' do
        expect(chef_run).to create_directory('/var/log/prometheus/alertmanager').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755',
          recursive: true
        )
      end

      it 'includes runit::default' do
        expect(chef_run).to include_recipe('runit::default')
      end

      it 'creates a cluster peer list' do
        expect(chef_run.node['alertmanager']['peers']).to eq(['192.168.1.2'])
      end

      it 'creates the configuration file with default content' do
        expect(chef_run).to create_template('/opt/prometheus/alertmanager/alertmanager.yml').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0644'
        )
        expect(chef_run).to(render_file('/opt/prometheus/alertmanager/alertmanager.yml').with_content do |content|
          expect { YAML.safe_load(content) }.not_to raise_error

          expect(content).to eq(IO.read('spec/fixtures/alertmanager.conf.template'))
        end)
      end

      it 'uploads all the alerting templates' do
        expect(chef_run).to create_remote_directory('/opt/prometheus/alertmanager/templates')
      end

      it 'installs the alertmanager' do
        expect(chef_run).to put_ark('alertmanager')
      end

      it 'does not downloads and decrypts file' do
        expect(chef_run).to_not create_gitlab_alertmanager_gcs_config('alertmanager')
      end

      it 'installs packages with the default action' do
        expect(chef_run).to install_package('curl')
        expect(chef_run).to install_package('tar')
        expect(chef_run).to install_package('bzip2')
      end

      it 'creates the runit service' do
        expect(chef_run).to enable_runit_service('alertmanager')
      end

      it 'does not download and decrypt file' do
        expect(chef_run).to_not run_execute('get alertmanager.yml.enc')
        expect(chef_run).to_not run_execute('decrypt alertmanager.yml.enc')
      end

      context 'with no peers' do
        cached(:chef_run) do
          ChefSpec::SoloRunner.new do |node|
            node.normal['prometheus-alertmanager']['alertmanager']['chef_vault'] = 'alertmanager'
            node.normal['prometheus']['alertmanagers_search'] = 'alertmanager_no_peers'
          end.converge(described_recipe)
        end

        it 'has no cluster peer list' do
          expect(chef_run.node['alertmanager']['peers']).to eq([])
        end
      end
    end
  end
end
