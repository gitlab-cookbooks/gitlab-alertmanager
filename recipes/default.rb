require 'yaml'

include_recipe 'ark::default'
include_recipe 'runit::default'

# Ensure the prometheus user exists
user node['prometheus']['user'] do
  system true
  shell '/bin/false'
  home "/opt/#{node['prometheus']['user']}"
  not_if node['prometheus']['user'] == 'root'
end

node.default['alertmanager']['gcs']['enable'] = (!node['cloud'].nil? && node['cloud']['provider'] == 'gce')

alertmanager_conf =
  if (secrets_hash = node['prometheus-alertmanager']['alertmanager']['secrets']) && !secrets_hash.empty?
    get_secrets(secrets_hash['backend'],
                secrets_hash['path'],
                secrets_hash['key'])['prometheus-alertmanager']['alertmanager']
  else
    # Fetch secrets from Chef Vault
    include_recipe 'gitlab-vault'
    GitLab::Vault.get(node, 'prometheus-alertmanager', 'alertmanager')
  end

peers_search_query = node['prometheus']['alertmanagers_search']
peers_search = search(:node, peers_search_query)

node.default['alertmanager']['peers'] =
  if peers_search.empty? || peers_search.none?
    []
  else
    get_alertmanager_peers(peers_search).sort.uniq
  end

directory node['alertmanager']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['alertmanager']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

%w(curl tar bzip2).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['alertmanager']['dir'])
dir_path = ::File.dirname(node['alertmanager']['dir'])

ark dir_name do
  url node['alertmanager']['binary_url']
  checksum node['alertmanager']['checksum']
  version node['alertmanager']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'runit_service[alertmanager]'
end

# Use the config template instead of GCS.
template node['alertmanager']['config']['file'] do
  source 'alertmanager.yml.erb'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  variables(conf: alertmanager_conf)
  not_if { node['alertmanager']['config']['gcs']['enable'] }
  notifies :hup, 'runit_service[alertmanager]'
end

# Setup a GCS compatible templates path and link.
directory '/etc/alertmanager' do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
end

link '/etc/alertmanager/templates' do
  to File.join(node['alertmanager']['dir'], 'templates')
  notifies :hup, 'runit_service[alertmanager]'
end

alertmanager_config_path = "gs://#{node['alertmanager']['config']['gcs']['bucket']}/#{node['alertmanager']['config']['gcs']['file']}"

# Get the file from GCS instead of the config template.
gitlab_alertmanager_gcs_config 'alertmanager' do
  path node['alertmanager']['config']['file']
  gcs_path alertmanager_config_path
  gcs_keyring node['alertmanager']['config']['kms']['keyring']
  gcs_keyring_project node['alertmanager']['config']['kms']['project']
  gcs_key node['alertmanager']['config']['kms']['key']
  only_if { node['alertmanager']['config']['gcs']['enable'] }
  retries 3
  retry_delay 5
  notifies :hup, 'runit_service[alertmanager]'
end

remote_directory node['alertmanager']['templates']['dir'] do
  source 'alertmanager/templates'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  notifies :hup, 'runit_service[alertmanager]'
end

runit_service 'alertmanager' do
  default_logger true
  log_dir node['alertmanager']['log_dir']
end
