module Gitlab
  module Alertmanager
    def get_alertmanager_peers(node_list)
      node_list.map do |node|
        first_ipv4_address = Gitlab.private_ips_for_node(node).first
        if (first_ipv4_address || '').empty?
          Chef::Log.warn("Node #{node['hostname']} does not have an ipv4 defined")
          next
        end
        first_ipv4_address
      end
    end
  end
end

Chef::Recipe.include Gitlab::Alertmanager
Chef::Resource.include Gitlab::Alertmanager
Chef::Provider.include Gitlab::Alertmanager
