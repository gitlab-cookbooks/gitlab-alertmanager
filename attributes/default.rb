#
# Cookbook:: GitLab::Monitoring
# Attributes:: default
#

default['prometheus']['user'] = 'prometheus'
default['prometheus']['group'] = 'prometheus'
default['prometheus']['alertmanagers_search'] = 'recipes:gitlab-alertmanager\\:\\:default'

default['alertmanager']['dir']              = '/opt/prometheus/alertmanager'
default['alertmanager']['templates']['dir'] = "#{node['alertmanager']['dir']}/templates"
default['alertmanager']['binary']           = "#{node['alertmanager']['dir']}/alertmanager"
default['alertmanager']['log_dir']          = '/var/log/prometheus/alertmanager'

default['alertmanager']['version'] = '0.20.0'
default['alertmanager']['checksum'] = '3a826321ee90a5071abf7ba199ac86f77887b7a4daa8761400310b4191ab2819'
default['alertmanager']['binary_url'] = "https://github.com/prometheus/alertmanager/releases/download/v#{node['alertmanager']['version']}/alertmanager-#{node['alertmanager']['version']}.linux-amd64.tar.gz"

default['alertmanager']['config']['file'] = "#{node['alertmanager']['dir']}/alertmanager.yml"

default['alertmanager']['config']['gcs']['bucket'] = 'gitlab-configs'
default['alertmanager']['config']['gcs']['enable'] = false
default['alertmanager']['config']['gcs']['file'] = 'alertmanager.yml.enc'
default['alertmanager']['config']['kms']['keyring'] = 'gitlab-shared-configs'
default['alertmanager']['config']['kms']['key'] = 'config'
default['alertmanager']['config']['kms']['project'] = 'gitlab-ops'

default['alertmanager']['flags']['config.file']        = node['alertmanager']['config']['file']
default['alertmanager']['flags']['web.listen-address'] = '0.0.0.0:9093'
default['alertmanager']['flags']['storage.path']       = "#{node['alertmanager']['dir']}/data"

default['alertmanager']['peers'] = []

# Make sure a slack channel is assigned for TEAM_alerts_channel and TEAM_low_priority_alerts_channel below.
default['alertmanager']['gitlab-teams'] = %w(
  ci-cd
  database
  gitaly
  observability
)

default['alertmanager']['slack']['api_url'] = nil
default['alertmanager']['slack']['channels'] = {
  main_alerts_channel: '#alerts',                              # The default route for alers
  pager_alerts_channel: '#production',                         # Alert on all pagerduties in #production
  'ci-cd_alerts_channel': '#alerts-ci-cd',                     # CI-CD high priority
  'ci-cd_low_priority_alerts_channel': '#alerts-ci-cd',        # CI-CD low priority
  database_alerts_channel: '#database',                        # Database high priority
  database_low_priority_alerts_channel: '#database',           # Database low priority
  gitaly_alerts_channel: '#g_gitaly',                          # Gitaly high priority
  gitaly_low_priority_alerts_channel: '#gitaly-alerts',        # Gitaly low priority
  observability_alerts_channel: '#observability',              # Observability high priority
  observability_low_priority_alerts_channel: '#observability', # Observability low priority
}

default['alertmanager']['snitch']['api_key'] = nil
default['alertmanager']['slack_bridge']['webhook_url'] = nil
default['alertmanager']['slack_bridge']['bearer_token'] = nil

default['alertmanager']['non_critical_pagerduty_environments'] = []
