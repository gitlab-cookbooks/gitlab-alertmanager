provides :gcs_config

property :path, String, name_property: true
property :gcs_path, String, required: true
property :gcs_keyring, String, required: true
property :gcs_keyring_project, String, required: true
property :gcs_key, String, required: true

action :create do
  encrypted_path = "#{new_resource.path}.enc"

  # Get the file from GCS
  execute 'get' do
    command "/usr/bin/gsutil cp #{new_resource.gcs_path} #{encrypted_path}"
  end

  # Decrypt the file
  execute 'decrypt' do
    command "/usr/bin/gcloud --project #{new_resource.gcs_keyring_project} kms decrypt --location global --keyring #{new_resource.gcs_keyring} --key #{new_resource.gcs_key} --ciphertext-file #{encrypted_path} --plaintext-file #{new_resource.path}"
  end
end
